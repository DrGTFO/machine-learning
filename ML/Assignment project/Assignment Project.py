
# coding: utf-8

# In[1]:


import seaborn as sns


# # Objective 1 and 2 and 3

# In[2]:


import pandas as pd
import numpy as np
df=pd.read_csv('IPL Match Data.csv')
del df['umpire3']
del df['id']
del df['date']
pd.set_option('display.max_rows', len(df))
df


# In[3]:


df['city'].fillna('Mumbai',inplace=True)
df=df.dropna(subset=['winner'])
df['player_of_match'].dropna(inplace=True)
df['umpire1'].fillna('HDPK Dharmasena',inplace=True)
df['umpire2'].fillna('SJA Taufel',inplace=True)
df['season']=(2017-df['season']-10)*-1
df


# In[4]:


df.describe(include='all')


# In[5]:


sns.boxplot(x='season',y='winner',data=df)


# In[6]:


a=df['team1'].astype('category')
df['team1']=a.cat.codes
cats=a.cat.categories
print(cats)
b=df['team2'].astype('category')
df['team2']=b.cat.codes
d=df['city'].astype('category')
df['city']=d.cat.codes
e=df['toss_winner'].astype('category')
df['toss_winner']=e.cat.codes
g=df['toss_decision'].astype('category')
df['toss_decision']=g.cat.codes
h=df['result'].astype('category')
df['result']=h.cat.codes
wins=h.cat.categories
print(wins)
df['player_of_match']=df['player_of_match'].astype('category').cat.codes
df['umpire1']=df['umpire1'].astype('category').cat.codes
df['umpire2']=df['umpire2'].astype('category').cat.codes
df['venue']=df['venue'].astype('category').cat.codes
c=df['winner'].astype('category')
df['winner']=c.cat.codes
df.corr()


# In[7]:


df.describe(include='all')


# # Objective 4 and 5 : Variance Threshold(.5)  and Splitting 
# 

# In[8]:


from sklearn.feature_selection import VarianceThreshold
from sklearn.model_selection import train_test_split
X=df.loc[:, df.columns != 'winner']
print(X.head(2))
y=df.loc[:, df.columns=='winner']
y_oth=df.winner
print(y_oth)
y.head(2)


# In[9]:


def variance_threshold_selector(data, threshold=0.65):
    selector = VarianceThreshold(threshold)
    selector.fit(data)
    return data[data.columns[selector.get_support(indices=True)]]


# In[10]:


X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=.30, random_state=0)
X_train_var = variance_threshold_selector(X_train,threshold=.5)  
X_test_var= variance_threshold_selector(X_test,threshold=.75)

print("X_Train :{} \nY_train : {}".format(X_train_var.shape,y_train.shape))


# In[11]:


X_train_var


# # Objective 6
# 

# In[12]:


#sns.pairplot(X_train_var, hue="team1", palette="Set2", diag_kind="kde")


# In[13]:


print(sns.countplot(df['team1']))


# In[14]:


print(sns.countplot(df['team2']))

Team 7 and team 2 played most games
# In[15]:


print(sns.countplot(df['winner']))

Team 7 won the most
Team 5 lost the most
# In[16]:


sns.countplot(df['season'])

Seson 2019 has most matches
# In[17]:


df.head(1)


# In[18]:


import matplotlib.pyplot as plt
plt.scatter(x=df['season'],y=df['winner'])
plt.title("season vs winner")
plt.show()
plt.scatter(x=df['city'],y=df['winner'])
plt.title("City vs winner")
plt.show()
plt.scatter(x=df['team1'],y=df['winner'])
plt.title("team1 vs winner")
plt.show()
plt.scatter(x=df['team2'],y=df['winner'])
plt.title("team2 vs winner")
plt.show()
plt.scatter(x=df['toss_winner'],y=df['winner'])
plt.title("toss_winner vs winner")
plt.show()
plt.scatter(x=df['toss_decision'],y=df['winner'])
plt.title("toss_decision vs winner")
plt.show()
plt.scatter(x=df['dl_applied'],y=df['winner'])
plt.title("dl_applied vs winner")
plt.show()
plt.scatter(x=df['venue'],y=df['winner'])
plt.title("venue vs winner")
plt.show()
plt.scatter(x=df['player_of_match'],y=df['winner'])
plt.title("player_of_match vs winner")
plt.show()
plt.scatter(x=df['umpire1'],y=df['winner'])
plt.title("umpire1 vs winner")
plt.show()
plt.scatter(x=df['umpire2'],y=df['winner'])
plt.title("umpire2 vs winner")
plt.show()


# # Objective 6
# 

# # Linear regression
# 

# In[19]:


from sklearn.linear_model import LinearRegression


# In[59]:


clf_linear=LinearRegression()
clf_linear.fit(X_train_var,y_train)
score_linear_train=clf_linear.score(X_train_var,y_train)
score_linear_test=clf_linear.score(X_test_var,y_test)
print("Train Score : {} , Test Score : {}".format(score_linear_train,score_linear_test))


# In[21]:


clf_linear.intercept_


# In[22]:


predict_linear=clf_linear.predict(X_test_var)
predict_linear


# In[23]:


from pandas import DataFrame,Series
coeff=DataFrame(X_train_var.columns)
coeff["Coeffient Estimate"]=Series(clf_linear.coef_.ravel())
coeff


# In[24]:


mse_linear=np.mean((predict_linear-y_test)**2)
np.sqrt(mse_linear)


# In[25]:


sns.regplot(x=predict_linear,y=y_test.values.ravel())


# # logistic Regression

# In[60]:


from sklearn.linear_model import LogisticRegression
clf_log=LogisticRegression()
clf_log.fit(X_train_var,y_train.values.ravel())
score_log_train=clf_log.score(X_train_var,y_train)
score_log_test=clf_log.score(X_test_var,y_test)
print("Train Score : {} , Test Score : {}".format(score_log_train,score_log_test))


# In[27]:


clf_log.intercept_


# In[28]:


predict_log=clf_log.predict(X_test_var)
predict_log


# In[29]:


mse_log=np.mean((predict_log-y_test.values.ravel())**2)
sqrt_log=np.sqrt(mse_log)
sqrt_log


# In[30]:


#sns.regplot(x=predict_log,y=y_test.values.ravel(),logistic=True)


# In[ ]:


`


# # Decision Tree 

# In[31]:


from sklearn.tree import DecisionTreeClassifier
clf_Dec=DecisionTreeClassifier(criterion='entropy',max_depth=4)
clf_Dec.fit(X_train_var,y_train.values.ravel())


# In[61]:


score_tree_train=clf_Dec.score(X_train_var,y_train)
score_tree_test=clf_Dec.score(X_test_var,y_test)
print("Train Score : {} , Test Score : {}".format(score_tree_train,score_tree_test))


# In[32]:


predict_Tree=clf_Dec.predict(X_test_var)
predict_Tree


# In[33]:


pd.DataFrame({'Feature':X_train_var.columns,'Importance':clf_Dec.feature_importances_})


# In[34]:


from sklearn.externals.six import StringIO
from IPython.display import Image  
from sklearn.tree import export_graphviz
import pydotplus
dot_data = StringIO()
export_graphviz(clf_Dec, out_file=dot_data,  
                filled=True, rounded=True,
                special_characters=True)
graph = pydotplus.graph_from_dot_data(dot_data.getvalue())  
Image(graph.create_png())


# # SVN 

# In[35]:


from sklearn.svm import SVC
clf_SVN_RBF=SVC()
clf_SVN_RBF.fit(X_train_var,y_train.values.ravel())


# In[62]:


score_SVN_RBF_train=clf_SVN_RBF.score(X_train_var,y_train)
score_SVN_RBF_test=clf_SVN_RBF.score(X_test_var,y_test)
print("Train Score : {} , Test Score : {}".format(score_SVN_RBF_train,score_SVN_RBF_test))


# In[36]:


from sklearn.svm import SVC
clf_SVN_Lin=SVC(kernel='linear',C=1)
clf_SVN_Lin.fit(X_train_var,y_train.values.ravel())
print("Train Score : {} , Test Score : {}".format(clf_SVN_Lin.score(X_train_var,y_train),clf_SVN_Lin.score(X_test_var,y_test)))


# In[63]:


score_SVN_Lin_train=clf_SVN_Lin.score(X_train_var,y_train)
score_SVN_Lin_test=clf_SVN_Lin.score(X_test_var,y_test)
print("Train Score : {} , Test Score : {}".format(score_SVN_Lin_train,score_SVN_Lin_test))


# In[37]:


clf_SVN_POL=SVC(kernel='poly',degree=2)
clf_SVN_POL.fit(X_train_var,y_train.values.ravel())
print("Train Score : {} , Tprint("Train Score : {} , Test Score : {}".format(clf_SVN_POL.score(X_train_var,y_train),clf_SVN_POL.score(X_test_var,y_test)))est Score : {}".format(clf_SVN_POL.score(X_train_var,y_train),clf_SVN_POL.score(X_test_var,y_test)))


# In[64]:


score_SVN_Lin_train=clf_SVN_POL.score(X_train_var,y_train)
score_SVN_Pol_test=clf_SVN_POL.score(X_test_var,y_test)
print("Train Score : {} , Test Score : {}".format(score_SVN_Lin_train,score_SVN_Pol_test))


# In[38]:


predict_SVN_RBF=clf_SVN_RBF.predict(X_test_var)
predict_SVN_Lin=clf_SVN_Lin.predict(X_test_var)
predict_SVN_Pol=clf_SVN_POL.predict(X_test_var)
print("RBF :{} \nLinear : {} \nPol:{}".format(predict_SVN_RBF,predict_SVN_Lin,predict_SVN_Pol))


# # KNN classifier 

# In[56]:


from sklearn.neighbors import KNeighborsClassifier
clf_neig=KNeighborsClassifier(n_neighbors=5)
clf_neig.fit(X_train_var,y_train.values.ravel())


# In[57]:


score_nei_train=clf_neig.score(X_train_var,y_train.values.ravel())
score_nei_test=clf_neig.score(X_test_var,y_test.values.ravel())
print("traint:{}, test:{}".format(score_nei_train,score_nei_test))


# In[72]:


predict_nei=clf_neig.predict(X_test_var)
print(y_test.values.ravel().shape)
print("predict:{}".format(predict_nei))


# #  Confusion Matrix

# In[88]:


from sklearn.metrics import confusion_matrix
#lin_confussion=confusion_matrix(y_test.values.ravel(),predict_linear)
Log_confussion=confusion_matrix(y_test.values.ravel(),predict_log)
tree_confussion=confusion_matrix(y_test.values.ravel(),predict_Tree)
RBF_confussion=confusion_matrix(y_test.values.ravel(),predict_SVN_RBF)
SVN_Lin_confussion=confusion_matrix(y_test.values.ravel(),predict_SVN_Lin)
Poly_confussion=confusion_matrix(y_test.values.ravel(),predict_SVN_Pol)
KNN_confussion=confusion_matrix(y_test.values.ravel(),predict_nei)
print("Classifier:                        Confusion Matrix")
print("""Linear Reg:                       \n{}\nLogistic Reg:                       \n{}\nDecision Reg:                       \n{}\n
SVN Lin Reg:                       \n{}\nSVN RBF Reg:                       \n{}\nSVN Pol Reg:                       \n{}\n
KNN Reg:                       \n{}\n
""".format("None",Log_confussion,tree_confussion,SVN_Lin_confussion,RBF_confussion,Poly_confussion,KNN_confussion))

